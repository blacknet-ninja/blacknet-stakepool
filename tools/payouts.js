



global.config = require('../config');

require('../lib/mongo');

let running = false;

async function processBlocks() {


    if (running) return;

    running = true;

    blocks = await Transaction.find({ type: 254, isProcess: false, blockHeight: { $gt: config.startHeight } });
    while (true) {

        let block = blocks.pop();

        if (!block) break;
        if (block.isProcess) continue;


        let height = block.blockHeight;
        let reward = block.data.amount;

        let leaseTxns = await Transaction.find({ blockHeight: { $lte: height - config.pos_mature_blocks }, type: 2 });
        let outLeases = await Transaction.find({ blockHeight: { $lte: height - config.pos_mature_blocks }, type: 3 });

        let payouts = getPayouts(leaseTxns, outLeases);
        for (let account in payouts) {

            if (payouts[account].shares < 1) continue;


            let amount = reward * (100 - config.fee) * (payouts[account].shares / payouts.allshares) / 100;

            if (account == 'allshares') continue;

            let payout = {
                account,
                reward,
                blockHeight: height,
                shares: payouts[account].shares,
                allShares: payouts.allshares,
                amount: amount,
                isPayout: false,
                fee: config.fee,
                poolOwner
            };
            let instance = new Payout(payout);
            await instance.save();
        }
        block.isProcess = true;
        await block.save();
    }

    running = false;
    console.log('payouts check over');
    setTimeout(processBlocks, 1000 * 30);

}

processBlocks();


function getPayouts(leaseTxns, cancelLeaseTxns) {

    let payouts = {}, allshares = 0;

    for (let tx of leaseTxns) {

        let address = tx.from;

        if (address == config.account) continue;

        if (!payouts[address]) {
            payouts[address] = {
                shares: 0
            };
        }

        payouts[address].shares += tx.data.amount;
        allshares += tx.data.amount;
    }


    for (let tx of cancelLeaseTxns) {

        let address = tx.from;

        if (address == config.account) continue;

        if (payouts[address]) {

            payouts[address].shares -= tx.data.amount;
            allshares -= tx.data.amount;
        }
    }

    payouts.allshares = allshares;
    return payouts;
}