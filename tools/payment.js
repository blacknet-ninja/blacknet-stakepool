
// 1. user api get all payment api,
// 2. check height confirm the same height with blnscan
// 3. sync api data & compare with DB data
// 4. make sure the payment is required
// 5. pay it && save it to blockchain

// message remark: PoS Reward Height:#447000


global.config = require('../config');
require('../lib/mongo');
const API = require('../lib/api');
const explorerAPI = require('../lib/explorerapi');
let currentHeight;
async function processPayment() {

    let page = 1;

    while (true) {
        console.log(`check payments page No.${page}`)
        let txns = await explorerAPI.getTransactions(poolOwner, 0, page);
        if (txns == undefined || txns.length === 0) break;

        await checkPayments(txns);

        if (page > 4) break;
        page++;
    }


    let startHeight = config.startHeight;
    let overview = await explorerAPI.getOverview();
    currentHeight = overview ? overview.height : currentHeight;
    let queryHeight = currentHeight - config.pos_confirmations;

    if (queryHeight < startHeight) return;


    while (true) {
        let heightQuery = { $gt: startHeight, $lt: queryHeight }, paySuccess = true;
        blocks = await Transaction.find({ isPayout: false, type: 254, blockHeight: heightQuery }).sort({ blockHeight: 'asc' }).limit(1000);

        if (blocks.length == 0 || blocks.length < 300) {
            console.log('all payment over');
            break;
        }

        let start = blocks[0].blockHeight, end = blocks[blocks.length - 1].blockHeight;

        console.log(`Start pay block: #${start}-${end}`);

        let payouts = await Payout.find({ isPayout: false, blockHeight: { $gte: start, $lte: end }, shares: { $gt: 0 }, amount: { $gt: 0.001 } });
        let pay = {}, messages = {};
        for (let payout of payouts) {

            if (parseInt(payout.amount) == 0) continue;
            if (parseInt(payout.shares) < 0) continue;
            if (payout.account == poolOwner) continue;

            if (!pay[payout.account]) {

                pay[payout.account] = payout.amount / 1e8;
                messages[payout.account] = {
                    startHeight: payout.blockHeight,
                    endHeight: payout.blockHeight
                };
            } else {
                pay[payout.account] += payout.amount / 1e8;
            }
        }

        console.log(' dont interrupt pay program...');

        for (let account in pay) {

            let amount = pay[account];
            let str = `${start} - ${end}`;
            let msg = 'PoS Reward Height:' + str;

            txid = await payIt(account, msg, amount * 1e8);

            if (txid && txid.length == 64) {
                await Payout.updateMany({ isPayout: false, account, blockHeight: { $gte: start, $lte: end }, shares: { $gt: 0 }, amount: { $gt: 0.001 } }, { isPayout: true, txid });
                console.log(`${account} pay success, payment is ${start}-${end}, \ntxid is ${txid}`)
            } else {
                paySuccess = false;
            }
            await timeout(1000 * 40);
        }

        if (paySuccess) {
            console.log('pay success...');

            await Transaction.updateMany({ isPayout: false, type: 254, blockHeight: { $gte: start, $lte: end } }, { isPayout: true });
        }
        await timeout(1000 * 40);
    }

    setTimeout(processPayment, 2 * 60 * 1000);
}

processPayment();


async function payIt(account, msg, amount) {

    let postdata = {
        mnemonic: mnemonic,
        amount: parseInt(amount),
        fee: 100000,
        to: account,
        message: msg,
        encrypted: 0
    };
    let txid = await API.sendMoney(postdata);
    console.log(`${account}, ${amount}, ${msg}\n${txid}`);

    return txid;
}


async function checkPayments(txns) {

    for (let tx of txns) {

        if (!tx.data) continue;
        let msg = tx.data.message, height, payout;

        if (msg.length == 0) continue;

        height = getHeightFromMsg(msg);
        if (/^\d+$/.test(height)) {
            // query DB
            let account = tx.to;

            payout = await Payout.findOne({ account, blockHeight: height })

            if (!payout) {
                continue;
            }
            if (payout.isPayout == false) {
                payout.isPayout = true;
                payout.txid = tx.txid;
                console.log(`${tx.txid} has been paid`)
                await payout.save();
            }
            let block = await Transaction.findOne({ blockHeight: height, type: 254 });
            block.isPayout = true;
            await block.save();
        } else {

            let blocks = getBlocksFromMsg(msg);

            if (blocks.length > 0) {
                await Transaction.updateMany({ blockHeight: { $in: blocks } }, { isPayout: true });
                await Payout.updateMany({ blockHeight: { $in: blocks }, account: tx.to }, { isPayout: true, txid: tx.txid });
            } else {

                let range = getBlocksFromRangeMsg(msg);
                if (range) {
                    await Transaction.updateMany({ isPayout: false, type: 254, blockHeight: { $gte: range.start, $lte: range.end } }, { isPayout: true });
                    await Payout.updateMany({ blockHeight: { $gte: range.start, $lte: range.end }, account: tx.to }, { isPayout: true, txid: tx.txid });
                }
            }
        }
    }
}



function getBlocksFromRangeMsg(msg) {

    let base = 'PoS Reward Height:', data = {};

    if (msg.indexOf(base) !== 0) return false;
    if (msg.replace(base, '').slice(0, 1) == '#') return false;

    let range = msg.replace(base, '');

    let arr = range.split(' - ');

    let start = arr[0], end = arr[1];

    if (/^\d+$/.test(start) && /^\d+$/.test(end)) {
        return { start, end };
    }

    return false;
}

function getBlocksFromMsg(msg) {

    let base = 'PoS Reward Height:', data = {};

    if (msg.indexOf(base) !== 0) return [];
    if (msg.replace(base, '').slice(0, 1) == '#') return [];

    try {
        msg = msg.replace(base, '');
        let arr = msg.split(',');
        if (arr.length == 1) return [];
        for (let key of arr) {
            data[key.split(':')[0]] = true;
        }
    } catch (e) {
        return [];
    }

    return Object.keys(data);
}


function getHeightFromMsg(msg) {

    let base = 'PoS Reward Height:#';

    return msg.replace(base, '');
}

function timeout(delay) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            try {
                resolve(1)
            } catch (e) {
                reject(0)
            }
        }, delay)
    })
}
