


require('../lib/mongo');


async function clearAllData() {

    await Transaction.deleteMany({});
    await Worker.deleteMany({});
    await Payout.deleteMany({});

    console.log('completed')
    process.exit(0)
}

setTimeout(clearAllData, 500);