Blacknet.controller('indexController', ['$scope', '$http', function ($scope, $http) {
    

    $scope.website_data = [];
    $http.get('/api/website_data').then(function (res) {
        $scope.website_data = res.data;
    })

}]);

Blacknet.controller('recent_blocks', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {

    $scope.txns = [];
    $scope.currentHeight = 0;
    function update(){
        $http.get('/api/recent_blocks').then(function (res) {
            $scope.currentHeight = res.data.overview.height;
            $scope.txns = res.data.txns.map(function(tx){
                
                return tx;
            });
        });
        $timeout(update, 1000 * 30);
    }
    update();

}]);

Blacknet.controller('workers', ['$scope', '$http', '$timeout',function ($scope, $http, $timeout) {

    $scope.workers = [];
    function update(){
        $http.get('/api/workers').then(function (res) {
            $scope.workers = res.data.sort(function(a, b){
                return parseInt(a.paid) - parseInt(b.paid) > 0 ? -1 : 1;
            });
        })
        $timeout(update, 1000 * 30);
    }
    update();
}]);

Blacknet.controller('payoutsController', ['$scope', '$http', function ($scope, $http) {
    $scope.payouts = {};
    $http.get('/api' + location.pathname).then(function (res) {
        $scope.payouts = res.data.filter(function(i){ return i.shares > 0});

        $scope.reward = res.data[0].reward;
        $scope.allShares = res.data[0].allShares;

    });
    
}]);

Blacknet.controller('transaction', ['$scope', '$http', function ($scope, $http) {
    $scope.tx = {
        amount: 0,
        fee: 0
    };
    $http.get('/api' + location.pathname).then(function (res) {
        $scope.tx = res.data;
    });
}]);

Blacknet.controller('blockController', ['$scope', '$http', function ($scope, $http) {
    $scope.block = {};
    $http.get('/api' + location.pathname).then(function (res) {
        res.data.transactions = res.data.transactions.map(function(tx){
            if(typeof tx.data == 'string') tx.data = JSON.parse(tx.data);
            return tx;
        });
        $scope.block = res.data;
    });
}]);

Blacknet.controller('account', ['$scope', '$http', function ($scope, $http) {

    $scope.payments = [];
    $scope.isloaded = false;
    $http.get('/api' + location.pathname ).then(function (res) {
        $scope.payments = res.data;
        $scope.isloaded = true;
    });

}]);

Blacknet.controller('recent_payouts', ['$scope', '$http', function ($scope, $http) {

    $scope.payments = [];

    $http.get('/api' + location.pathname ).then(function (res) {
        $scope.payments = res.data;
    });

}]);



Blacknet.controller('bodyController', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {

    let lang = navigator.language || navigator.userLanguage, locale = '';

    function toggleNav() {
        let display = document.querySelector('.nav').style.display;

        display = display != 'block' ? 'block': 'none';
        document.querySelector('.nav').style.display = display;
    }
    $scope.toggleNav = toggleNav;
    // $scope.BlockTime = BlockTime;
    
    if (lang.indexOf('zh') !== -1) {
        locale = 'zh_CN';
    } else if (lang.indexOf('ja') !== -1) {
        locale = 'ja';
    } else if (lang.indexOf('sk') !== -1) {
        locale = 'sk';
 // } else if (lang.indexOf('ru') !== -1) {
 //     locale = 'ru';
    } else if (lang.indexOf('de') !== -1) {
        locale = 'de';
    }
    if (locale) {
        $http.get('/i18n/' + locale + '.json').then(function (res) {
            let data = res.data;
            for (let key in data) {
                data[key.toLowerCase()] = data[key];
            }
            window.i18nData = data
            $(document).find('[data-i18n]').each(function () {

                let el = $(this), key = el.data('i18n').toLowerCase();

                if (data[key]) {
                    el.text(data[key]);
                }
            });
            
            $timeout(function(){
                $('.dialog').hide();
            }, 200);

        });
    }else{
        $timeout(function(){
            $('.dialog').hide();
        }, 200);
    }
}]);

