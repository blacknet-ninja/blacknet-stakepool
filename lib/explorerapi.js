

const host = 'https://blnscan.loqunbai.com/api';
const request = require("request");

module.exports = {

    getTransactions: function (address, type, page) {

        return getApi(`/pool/${address}?type=${type}&page=${page}`);
    },

    getRecentBlocks: function () {

        return getApi(`/recent_blocks`);

    },
    getNetworkHash: function () {
        return getApi('/networkhashrate');
    },
    getOverview: function () {
        return getApi('/ledger');
    }
}

function getApi(url) {
    return new Promise((resolve) => {
        request({
            url: host + url,
            headers: {
                'User-Agent': 'blacknet-stakepool'
            },
            forever: true,
            json: true
        }, (err, res, body) => {
            resolve(body);
        });
    });
}



















